import { Component, OnInit } from "@angular/core";
import { GraficasService } from "../../services/graficas.service";

@Component({
  selector: "app-graficas",
  templateUrl: "./graficas.component.html"
})
export class GraficasComponent implements OnInit {
  constructor(private _graficasService: GraficasService) {}
  sistemas: any = [];
  modulos: any = [];
  data: any = [];
  arrayOrdered: any = [];
  arrayGrafica: any = [];
  listaRemotos: any;

  ngOnInit() {
    this.searchOthers();
  }

  searchOthers() {
    this._graficasService.remotos().subscribe(
      (res: any) => {
        this.listaRemotos = res;
        this.searchData();
      },
      err => {
        console.log(err);
      }
    );
  }

  changeUrl(index) {
    //this._racooService.createUrl(index);
    this.searchData(index);
  }

  searchData(index?: any) {
    this._graficasService.datos(index).subscribe(
      (res: any) => {
        this.cycled(res);
        //console.log(res);
      },
      err => {
        console.log(err);
      }
    );
  }

  cycled(data) {
    this.lineChartLabels = data.map(semana => semana.ejeX);

    this.getKeys(data);
    //console.log(this.data.length / this.lineChartLabels.length);

    this.data.forEach(valueOne => {
      let otherArray = this.data.filter(valueTwo => {
        if (
          valueOne.sub_modulo === valueTwo.sub_modulo &&
          valueOne.modulo === valueTwo.modulo &&
          valueOne.sistema === valueTwo.sistema
        ) {
          return valueTwo;
        }
      });

      if (
        this.arrayOrdered.length <
        this.data.length / this.lineChartLabels.length
      ) {
        this.arrayOrdered.push(otherArray);
      }
    });

    //console.log(this.arrayOrdered);

    this.arrayOrdered.forEach(values => {
      let lineChartData = [{ data: [], label: "", sistema: "", modulo: "" }];

      let newArray = values.map(value => {
        return value;
      });

      for (let i = 0; i < newArray.length; i++) {
        lineChartData[0].sistema = newArray[0].sistema;
        lineChartData[0].modulo = newArray[0].modulo;
        lineChartData[0].label = newArray[0].sub_modulo;
        lineChartData[0].data.push(newArray[i].valor);
      }
      //console.log(newArray);
      //console.log(lineChartData);
      //console.log("Fin de 1er ciclo");

      this.lineChartData.push(lineChartData);
    });
  }

  getKeys(object) {
    //accedemos al objeto raiz
    let keys = Object.values(object);
    keys.map((key: any) => {
      //Accedemos dentro del segundo nivel de objetos anidados
      let sistemas = Object.values(key.sistema);
      sistemas.map((sistema: any) => {
        //Validamos los sistemas que existen y los guardamos en un array
        if (this.sistemas.indexOf(sistema.nombre) == -1) {
          this.sistemas.push(sistema.nombre);
        }

        //Acceder a los modulos de cada sistema
        let modulos = Object.values(sistema.modulos);
        modulos.map((modulo: any) => {
          //Validamos los modulos que existen y los guardamos en un array
          if (this.modulos.indexOf(modulo.nombre) == -1) {
            this.modulos.push(modulo.nombre);
          }

          //Accedemos a los submodulos de cada modulo
          let sub_modulos = Object.values(modulo.subModulos);
          sub_modulos.map((sub_modulo: any) => {
            this.data.push({
              sistema: sistema.nombre,
              modulo: modulo.nombre,
              sub_modulo: sub_modulo.subNombre,
              valor: sub_modulo.valor
            });
          });
        });
      });
    });
  }

  public lineChartData: Array<any> = [
    /*
    { data: [19, 20, 23, 35, 37], label: "Nuevos" }
    */
  ];

  public lineChartLabels: Array<any> = [
    /*"Enero",
    "Febrero",
    "Marzo",
    "Abril",
    "Mayo",
    "Junio",
    "Julio"*/
  ];

  public lineChartOptions: any = {
    responsive: true
  };

  public lineChartColors: Array<any> = [
    {
      // blue
      backgroundColor: "rgba(148,159,177,0.1)",
      borderColor: "rgba(56,36,255,1)",
      pointBackgroundColor: "rgba(148,159,177,1)",
      pointBorderColor: "#fff",
      pointHoverBackgroundColor: "rgba(56,36,255,1)",
      pointHoverBorderColor: "rgba(148,159,177,0.8)"
    }
  ];

  public lineChartLegend: boolean = true;
  public lineChartType: string = "line";

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }
}
