import { Component, OnInit } from "@angular/core";
import { RacooService } from "../../services/racoo.service";

@Component({
  selector: "app-inicio",
  templateUrl: "./inicio.component.html"
})
export class InicioComponent implements OnInit {
  url: string = this._racooService.url;
  listaUrl: string = this._racooService.listaUrl;
  navActive: string = "";
  res: any;
  data: any;
  info: boolean = false;
  response: boolean = false;
  listaRemotos: any;

  constructor(private _racooService: RacooService) {}

  ngOnInit() {
    this.searchOthers();
    //this.searchData();
  }

  searchOthers() {
    this._racooService.remotos().subscribe(
      (res: any) => {
        this.listaRemotos = res;
        this.searchData();
      },
      err => {
        console.log(err);
      }
    );
  }

  changeUrl(index) {
    //this._racooService.createUrl(index);
    this.searchData(index);
  }

  searchData(index?: any) {
    this._racooService.datos(index).subscribe(
      (res: any) => {
        this.data = res.data;
        this.url = res.url;
        this.select(0);

        //console.log(res);
      },
      err => {
        console.log(err);
      }
    );
  }

  select(index: number) {
    this.navActive = this.data.sistema[index].nombre;
    this.res = this.data.sistema[index].modulos;
    //console.log("respuesta", this.res);
  }

  showInfo() {
    this.info = !this.info;
  }

  porcentaje(value: any, criterio: any) {
    if (value === null) {
      return {
        valor: true,
        porcentaje: 100,
        mensaje: "No viene valor de origen"
      };
    }

    let resultado = Math.round((value / criterio) * 100);
    if (resultado >= 100) {
      return 100;
    }

    if (Number.isNaN(resultado)) {
      if (!Number.isInteger(value)) {
        value = value.split("%");
        value = Number.parseInt(value);
      }

      criterio = criterio.split("%");
      criterio = Number.parseInt(criterio);
      resultado = Math.round((value * 100) / criterio);
      if (resultado >= 100) {
        return 100;
      }
    }

    return resultado;
  }

  showResponse() {
    this.response = !this.response;
  }
}
