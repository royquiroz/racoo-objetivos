import { Component, OnInit } from "@angular/core";
import { TablaService } from "../../services/tabla.service";

@Component({
  selector: "app-tabla",
  templateUrl: "./tabla.component.html"
})
export class TablaComponent implements OnInit {
  data: any;
  columnas: any;
  notarias: any;
  dataTable: any;
  response: boolean = false;
  listaRemotos: any;

  constructor(private _tablaService: TablaService) {}

  ngOnInit() {
    this.searchOthers();
  }

  searchOthers() {
    this._tablaService.remotos().subscribe(
      (res: any) => {
        this.listaRemotos = res;
        this.searchData();
      },
      err => {
        console.log(err);
      }
    );
  }

  changeUrl(index) {
    //this._racooService.createUrl(index);
    this.searchData(index);
  }

  searchData(index?: any) {
    this._tablaService.datos(index).subscribe(
      (res: any) => {
        this.data = res;
        this.columnas = res.columnas;
        this.notarias = res.filas;

        this.transformData(this.notarias);
        //console.log(res);
      },
      err => {
        console.log(err);
      }
    );
  }

  transformData(data: any) {
    let arrayNotarias: any = [];

    data.map(notaria => {
      let object: any = {};
      let valores: any = [];
      object.notaria = notaria.notaria;
      object.fecha = notaria.fecha;
      this.columnas.map(columna => {
        valores.push(notaria.valores[columna]);
      });
      object.valores = valores;

      arrayNotarias.push(object);
    });
    console.log(arrayNotarias);
    this.dataTable = arrayNotarias;
  }

  showResponse() {
    this.response = !this.response;
  }
}
