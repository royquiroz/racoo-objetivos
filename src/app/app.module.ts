import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";

import { HttpClientModule } from "@angular/common/http";

import { APP_ROUTING } from "./app.routes";

//Components
import { AppComponent } from "./app.component";
import { InicioComponent } from "./components/inicio/inicio.component";

//Graficos
import { ChartsModule } from "ng2-charts";

//Pipes
import { ObjectsPipe } from "./pipes/objects.pipe";
import { RutasPipe } from "./pipes/rutas.pipe";
import { CapitalizadoPipe } from "./pipes/capitalizado.pipe";
import { GraficasComponent } from "./components/graficas/graficas.component";
import { NavbarComponent } from "./components/shared/navbar/navbar.component";
import { TablaComponent } from './components/tabla/tabla.component';

@NgModule({
  declarations: [
    AppComponent,
    InicioComponent,
    ObjectsPipe,
    RutasPipe,
    CapitalizadoPipe,
    GraficasComponent,
    NavbarComponent,
    TablaComponent
  ],
  imports: [BrowserModule, HttpClientModule, APP_ROUTING, ChartsModule, NgbModule.forRoot()],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
