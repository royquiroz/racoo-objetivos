import { RouterModule, Routes } from "@angular/router";

import { InicioComponent } from "./components/inicio/inicio.component";
import { GraficasComponent } from "./components/graficas/graficas.component";
import { TablaComponent } from "./components/tabla/tabla.component";

// Components

const APP_ROUTES: Routes = [
  { path: "inicio", component: InicioComponent },
  { path: "graficas", component: GraficasComponent },
  { path: "tabla", component: TablaComponent },
  { path: "**", pathMatch: "full", redirectTo: "inicio" }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, { useHash: false });
