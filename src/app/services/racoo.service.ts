import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { map, catchError } from "rxjs/operators";
import { Observable, throwError } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class RacooService {
  listaRemotos: any = [];
  dataUrl: string = "";
  url: string = "";
  listaUrl: string;

  apiUrl: string =
    "http://minotaria.net/base/apps/api/public/index.php/criteriosExito/todos/v3/random";

  remotosUrl: string =
    "http://minotaria.net/base/apps/api/public/index.php/criteriosExito/todos/v3/listar";

  constructor(private http: HttpClient) {
    console.log("Racoo service Listo");
  }

  remotos(): Observable<any> {
    return this.http.get(/*this.remotosUrl*/ this.remotosLista()).pipe(
      map((data: any) => {
        this.listaRemotos = data;
        return data;
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  remotosLista() {
    this.listaUrl = `${this.createPath()}/apps/api/public/index.php/criteriosExito/todos/v3/listar`;

    console.log("ruta lista: ", this.listaUrl);

    return this.listaUrl;
  }

  datos(index?: any): Observable<any> {
    return this.http.get(this.createUrl(index) /*this.apiUrl*/).pipe(
      map((data: any) => {
        return { data, url: this.url };
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  createUrl(index?: any) {
    if (index == 0 || index == "" || index == undefined) {
      this.url = `${this.createPath()}/${this.listaRemotos[0].url}`;
      console.log("Ruta a conectar", this.url);
      return this.url;
    }
    this.url = this.listaRemotos[index].url;
    console.log("Ruta a conectar", this.url);
    return this.url;
  }

  createPath() {
    let pathname = window.location.pathname.split("/angular/todos/");
    let pathnameNew = [];
    //console.log(pathname);

    for (let i = 0; i < pathname.length; i++) {
      if (pathname[i] !== "") {
        pathnameNew.push(pathname[i]);
      }
    }

    if (
      pathnameNew[0] === "/" ||
      pathnameNew[0] === undefined ||
      pathnameNew[0] === "/inicio" ||
      pathnameNew[0] === "inicio"
    ) {
      pathnameNew[0] = "";
      console.log(pathnameNew);
    }
    return `http://${window.location.host}${pathnameNew[0]}`;
  }
}
