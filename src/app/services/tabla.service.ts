import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { map, catchError } from "rxjs/operators";
import { Observable, throwError } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class TablaService {
  listaRemotos: any = [];
  url: string = "";
  listaUrl: string;

  apiUrl: string =
    "http://minotaria.net/base/apps/api/public/index.php/criteriosExito/tabla";

  constructor(private http: HttpClient) {
    console.log("Tabla service Listo");
  }

  remotosLista() {
    this.listaUrl = `${this.createPath()}/apps/api/public/index.php/criteriosExito/tabla/listar`;

    console.log("ruta lista: ", this.listaUrl);

    return this.listaUrl;
  }

  remotos(): Observable<any> {
    return this.http.get(/*this.remotosUrl*/ this.remotosLista()).pipe(
      map((data: any) => {
        this.listaRemotos = data;
        return data;
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  datos(index?: any): Observable<any> {
    return this.http.get(/*this.apiUrl*/ this.createUrl(index)).pipe(
      map((data: any) => {
        return data;
      }),
      catchError(err => {
        return throwError(err);
      })
    );
  }

  createUrl(index?: any) {
    if (index === "" || index === undefined) {
      index = 0;
    }

    if (!this.listaRemotos[index].url.includes("http")) {
      this.url = `${this.createPath()}/${this.listaRemotos[index].url}`;
      console.log("Ruta a conectar", this.url);
      return this.url;
    }
    this.url = this.listaRemotos[index].url;
    console.log("Ruta a conectar", this.url);
    return this.url;
  }

  createPath() {
    let pathname = window.location.pathname.split("/angular/todos/");
    let pathnameNew = [];
    //console.log(pathname);

    for (let i = 0; i < pathname.length; i++) {
      if (pathname[i] !== "") {
        pathnameNew.push(pathname[i]);
      }
    }

    if (
      pathnameNew[0] === "/" ||
      pathnameNew[0] === undefined ||
      pathnameNew[0] === "/inicio" ||
      pathnameNew[0] === "inicio"
    ) {
      pathnameNew[0] = "";
      console.log(pathnameNew);
    }
    return `http://${window.location.host}${pathnameNew[0]}`;
  }
}
