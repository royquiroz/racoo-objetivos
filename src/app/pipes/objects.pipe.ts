import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "objects"
})
export class ObjectsPipe implements PipeTransform {
  transform(value: any): any {
    if (value === undefined || value === null) {
      return null;
    }
    return Object.keys(value);
  }
}
