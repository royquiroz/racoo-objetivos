import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "rutas"
})
export class RutasPipe implements PipeTransform {
  transform(value: any): any {
    return value.split(" ").join("-");
  }
}
